const makeChessboard = () => {
  let chessboard = []

  // ... write your code here
  let bidak = ["B", "K", "M", "RT", "RJ", "M", "K", "B"]; // membuat suatu array berisi urutan bidak catur
  for(let i = 0; i < 8; i++) {
    let baris = [];
    for(let j = 0; j < 8; j++) {
      if(i == 0)
        baris.push(bidak[j] + 'Black'); // kondisi ketika di baris 0, maka menambahkan urutan array bidak catur dengan string Black
      else if(i == 1)
        baris.push('P Black'); // kondisi ketika di baris 1, maka array kosong akan diisi semua oleh P Black
      else if(i == 6)
        baris.push('P White'); // kondisi ketika di baris 6, maka array kosong akan diisi semua oleh P White
      else if(i == 7)
       baris.push(bidak[j] + 'White'); // kondisi ketika di baris 7, maka menambahkan urutan array bidak catur dengan string White
      else
       baris.push('-'); // sisa dari baris akan diisi oleh -
    }

    chessboard.push(baris);
  }
  return chessboard
}

const printBoard = x => {
  // ... write your code here
  for(let i = 0; i < x.length; i++)
    console.log(x[i]);
}

printBoard(makeChessboard())